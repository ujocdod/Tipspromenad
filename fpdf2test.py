import tkinter
from fpdf import FPDF
from PIL import Image, ImageTk
from tkinter import filedialog
import customtkinter as ctk
import pickle
import random


questions = [['', 'Vad blir tre gånger fyra?', 'Tolv', 'Fyra', 'Arton']]
page = 0

#Ingvar = tkinter.StringVar(pg_nr)

#Skapar fönster
win = ctk.CTk()
win.resizable(False, False)
win.title("window")
win.geometry('550x435')
win.attributes('-topmost',True)

#image1 = Image.open("no_photo.jpg")

#test = ImageTk.PhotoImage(Image.open('2x1.png').resize((300,150)))

#Bildobjektet
test = ImageTk.PhotoImage(Image.open('2x1.png'))

#Rutor för finfint GUI att bo i
t_frame = ctk.CTkFrame(win, width=820, height=110)
b_frame = ctk.CTkFrame(win, width=520, height=100)
l_frame = ctk.CTkFrame(t_frame, width=100, height=210)
m_frame = ctk.CTkFrame(t_frame, width=310, height=210)
r_frame = ctk.CTkFrame(t_frame, width=100, height=210)
tb_frame = ctk.CTkFrame(b_frame, width=550, height=40)
bb_frame = ctk.CTkFrame(b_frame, width=550, height=200)

#Ettiketen med bilden i
ram = tkinter.Label(master = m_frame, image=test, borderwidth=0)


#print(questions[page][0])

#pdf.set_font('times', 'B')

#define functions

def change_pg_nr():
    pg_nr.configure(text=f"{page + 1}/{len(questions)}")

def add_page():
    questions.append(['', 'A' ,'Brand', 'New', 'Page!'])
    change_pg_nr()
    print(questions)

#Byter ut/lägger till en bild, ska ändra hur avsaknad av vald bild hanteras
def load_img():
    global test
    global questions
    global page
    global ram
    img_name = questions[page][0]
    print(img_name)
    if img_name == '':
        print('noll')
        img_name = 'no_photo.jpg'

    im = Image.open(img_name)

#Lägger in bilden, horisontelt centrerat om den är hög och vertikalt centrerad om den är bred
    w = im.width
    h = im.height
    ratio = w / h
    if ratio >= 1.5:
        test = ImageTk.PhotoImage(Image.open(img_name).resize((300, round(300 / ratio))))
        ram.pack(side='left', padx=5)
    else:
        test = ImageTk.PhotoImage(Image.open(img_name).resize((round(200 * ratio), 200)))
        ram.pack(pady=5, side='top')

    ram.configure(image=test)

#Gammal funktion som inte har använts på länge
def width_ratio(image_path):
    im = Image.open(image_path)
    ratio = (im.width / im.height)
    return ratio

#Öppnar fildialoggrej för att välja bild
def image_add():
    global questions
    im = str(tkinter.filedialog.askopenfilename(initialdir = "C:\\Users", title = "Choose an image", filetypes = (("Images", ".png"), ("all files", "*.*"))))
    print(im)
    questions[page][0] = im
    load_img()

def image_remove():
    global questions
    questions[page][0] = ''
    load_img()

#Uppdaterar skrivrutor när man bläddrar mellan sidor
def read_list():
    q_ntr.delete(0, len(q_ntr.get()))
    x_ntr.delete(0, len(x_ntr.get()))
    y_ntr.delete(0, len(y_ntr.get()))
    z_ntr.delete(0, len(z_ntr.get()))

    q_ntr.insert(0, questions[page][1])
    x_ntr.insert(0, questions[page][2])
    y_ntr.insert(0, questions[page][3])
    z_ntr.insert(0, questions[page][4])

    load_img()

#Bläddrar till nästa sida
def nxt():
    global page
    write_list()
    if page == len(questions) - 1:
        page = 0
    else:
        page = page + 1
    print(page)
    change_pg_nr()
    read_list()

#Bläddrar till föregående sida
def pre():
    global page
    write_list()
    if page == 0:
        page = len(questions) - 1
    else:
        page = page - 1
    print(page)
    change_pg_nr()
    read_list()

#Sparar skrivrutornas innehåll
def write_list():
    questions[page][1] = q_ntr.get()
    questions[page][2] = x_ntr.get()
    questions[page][3] = y_ntr.get()
    questions[page][4] = z_ntr.get()


#def insert_center_image(height=100, page_width=210 ):
    #pdf = FPDF('P', 'mm', 'A4')
    #pdf.add_page()
    #pdf.set_font('helvetica', size=300)
    #if width_ratio(img_name) > 180:
        #im = Image.open(img_name)
        #filler_height = (100 - 18000 / width_ratio(img_name)) / 2 + 50
        #print(filler_height)
        #pdf.image(img_name, x=15, y=filler_height, w=180)
    #else:
        #pdf.image(img_name, h=height, x=(page_width - (width_ratio(img_name))) / 2, y=50)
    #pdf.output('morot.pdf')

#Skapar en pdf med bilder, frågor och alternativ
def save_pdf():
    global questions
    write_list()
    pdf = FPDF('P', 'mm', 'A4')
    for n, x in enumerate(questions):
        im = Image.open(x[0])
        ratio = (im.width / im.height)
        print(x)
        pdf.add_page()
        pdf.set_font('helvetica', size=100)
        pdf.cell(w=0, txt=(f"{n + 1}."), border=0, align='C', fill=False, h=30)

#Räknar ut vilka mått bilden ska ha och lägger till den i PDF:en
        if ratio >= 2:
            pdf.image(x[0], x=15, y=((90-180/ratio)/2+50), w=180)
        else:
            pdf.image(x[0], y=50, x=((180 - 90 * ratio) / 2 + 15), h=90)

        pdf.set_y(145)

#Fråga
        pdf.set_font('helvetica', size=35, style='B')
        pdf.multi_cell(w=0, txt=x[1], border=0, align='C', fill=False, h=12)
#Alternativ i slumpad ordning
        order = [2, 3, 4]
        random.shuffle(order)
        option_names = ['1:', 'X:', '2:']


        for i, c in enumerate(order):
            pdf.set_y(pdf.y + 3)
            pdf.set_font('helvetica', size=30)
            pdf.multi_cell(w=0, txt=((option_names[i])+(x[c])), border=0, align='C', fill=False, h=10)

    files = [('PDF', '*.pdf')]
    file = tkinter.filedialog.asksaveasfile(filetypes=files, defaultextension=files)
    file = file.name

    pdf.output(file)


#Ladda tipsprommenad från picklefil
def load():
    global questions
    global page
    file = str(tkinter.filedialog.askopenfilename(initialdir="C:\\Users", title="Choose an image",
                                                filetypes=(("Pickle", ".pkl"), ("all files", "*.*"))))
    print(file)
    if file != '':
        with open(file, 'rb') as f:
            questions = pickle.load(f)
        page = 0
        load_img()
        read_list()
        change_pg_nr()

#Spara tipspromenad som picklefil
def save():
    files = [('All Files', '*.*'),
        ('Pickle', '*.pkl'),
        ('Text Document', '*.txt')]
    file = tkinter.filedialog.asksaveasfile(filetypes=files, defaultextension=files)
    file = file.name

    with open(file, 'wb') as f:
        pickle.dump(questions, f)



#Bygger klart fönstret

t_frame.pack(padx=5, pady=5)
l_frame.pack(side='left', padx=5, pady=5)
m_frame.pack(side='left', padx=5, pady=5)
m_frame.pack_propagate(False)
r_frame.pack(side='left', padx=5, pady=5)
b_frame.pack(padx=5, pady=5)
tb_frame.pack(padx=5, pady=5)
bb_frame.pack(padx=5, pady=5)


#ram = tkinter.Label(master = m_frame, image=test, borderwidth=0)
#ram.pack(padx=5, side='left')

img_btn = ctk.CTkButton(master=tb_frame, text ='Add image', command= image_add, width=100)
img_remove_btn = ctk.CTkButton(master=tb_frame, text ='X', command= image_remove, width=30)
pdf_btn = ctk.CTkButton(master=tb_frame, text ='Save .pdf', command= save_pdf, width=100)
load_btn = ctk.CTkButton(master=tb_frame, text ='Load', command= load, width=100)
save_btn = ctk.CTkButton(master=tb_frame, text ='Save', command= save, width=100)

img_btn.pack(padx=5, pady=5, side='left')
img_remove_btn.pack(padx=5, pady=5, side='left')
pdf_btn.pack(padx=5, pady=5, side='left')
load_btn.pack(padx=5, pady=5, side='left')
save_btn.pack(padx=5, pady=5, side='left')

q_ntr = ctk.CTkEntry(bb_frame, width=540)
x_ntr = ctk.CTkEntry(bb_frame, width=540)
y_ntr = ctk.CTkEntry(bb_frame, width=540)
z_ntr = ctk.CTkEntry(bb_frame, width=540)

q_ntr.pack(padx=5, pady=3)
x_ntr.pack(padx=5, pady=3)
y_ntr.pack(padx=5, pady=3)
z_ntr.pack(padx=5, pady=3)


pg_btn = ctk.CTkButton(r_frame, width=90, height=90, text='  Add \n page', command=add_page, font=('roboto', 20))
nxt_btn = ctk.CTkButton(r_frame, width=90, height=8, text='>', font=('roboto', 80), command=nxt)

pg_nr = ctk.CTkLabel(l_frame, width=90, height=90, text='1/1', font=('roboto', 20))
pre_btn = ctk.CTkButton(l_frame, width=90, height=90, text='<', font=('roboto', 80), command=pre)

pg_btn.pack(padx=5, pady=3)
nxt_btn.pack(padx=5, pady=3)

pg_nr.pack(padx=5, pady=3)
pre_btn.pack(padx=5, pady=3)

load_img()


#bildram = tkinter.Label(master=m_frame ,image=test)
#bildram.pack()


#pdf.cell(0, 80, txt="text", align='C',new_x='LMARGIN', new_y='NEXT')
#pdf.set_font('helvetica', size=150)
#pdf.cell(0, 40, txt="vet inte", align='C',new_x='LMARGIN', new_y='NEXT')
#pdf.image('normal.jpg', h=100, x = (210 - (width_ratio('normal.jpg'))) / 2)

read_list()

win.mainloop()
